# Telios Desktop Email Client

![client_screenshot.png](repo/client_screenshot.png)

<br>

Telios is an offline-capabale e2e encrypted email service built on [hypercore-protocol](https://hypercore-protocol.org/) for sending and receiving emails.

<br>

<div align="center">

</div>

## ⚠️ This project is not production ready ⚠️

Features are still being built and optimized. While functional in it's current state, it is not recommended for regular use.

## Starting Development

Start the app in the `dev` environment:

```bash
yarn start
```

## Packaging for Production

To package apps for the local platform:

```bash
yarn package
```

## Docs

See our [docs here](https://telios2.gitlab.io/telios-docs/docs/intro)

## License

CC-BY-NC-4.0
